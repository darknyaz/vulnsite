from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from werkzeug.exceptions import abort
from vulnsite.auth import login_required
from vulnsite.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    """ Show all the posts, most recent first. """

    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created_time, author_id, nickname '
        'FROM post p JOIN user u ON p.author_id = u.id '
        'ORDER BY created_time DESC'
    ).fetchall()

    return render_template('blog/index.html', posts=posts)


def get_post(id, check_author=True):
    """
    Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """

    post = (
        get_db()
        .execute(
            'SELECT p.id, title, body, created_time, author_id, nickname '
            'FROM post p JOIN user u ON p.author_id = u.id '
            'WHERE p.id = ?',
            (id, )
        )
        .fetchone()
    )

    if post is None:
        abort(404, 'Post id {0} doesn\'t exist.'.format(id))

    if check_author and post['author_id'] != session.get('user_id'):
        abort(403)

    return post


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    """ Create a new post for the current user. """

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id) VALUES (?, ?, ?)',
                (title, body, session['user_id']),
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


@bp.route('/update', methods=('GET', 'POST'))
@login_required
def update():
    """ Update a post if the current user is the author. """

    post_id = request.args.get('post')
    post = get_post(post_id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ? WHERE id = ?',
                (title, body, post_id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/delete', methods=('POST',))
@login_required
def delete():
    """
    Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """

    post_id = request.args.get('post')
    get_post(post_id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (post_id, ))
    db.commit()
    return redirect(url_for('blog.index'))
