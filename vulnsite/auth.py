from functools import wraps
from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash
from vulnsite.db import get_db

bp = Blueprint('auth', __name__)

def login_required(view):
    """ View decorator that redirects anonymous users to the login page. """

    @wraps(view)
    def wrapped_view(**kwargs):
        if 'user_id' not in session:
            return redirect(url_for('auth.login'))
        return view(**kwargs)

    return wrapped_view


@bp.route('/register', methods=('GET', 'POST'))
def register():
    """ Register a new user. """

    if request.method == 'POST':
        nickname = request.form['nickname']
        password = request.form['password']
        db = get_db()
        error = None

        if not nickname:
            error = 'Nickname is required.'
        elif not password:
            error = 'Password is required.'
        elif (
            db.execute(
                'SELECT id FROM user WHERE nickname = ?', (nickname, )
            ).fetchone() is not None
        ):
            error = 'User {0} is already registered.'.format(nickname)

        if error is None:
            db.execute(
                'INSERT INTO user (nickname, password) VALUES (?, ?)',
                (nickname, generate_password_hash(password))
            )
            db.commit()
            return redirect(url_for('.login'))

        flash(error)

    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    """ Log in a registered user by adding the user id to the session. """

    if request.method == 'POST':
        nickname = request.form['nickname']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
                'SELECT * FROM user WHERE nickname = ?', (nickname, )
            ).fetchone()

        if user is None:
            error = 'Incorrect nickname.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            session['user_nickname'] = db.execute(
                    'SELECT nickname FROM user WHERE id = ?', (user['id'], )
                ).fetchone()[0]

            print(session['user_nickname'])

            return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')


@bp.route('/logout')
def logout():
    """ Clear the current session. """

    session.clear()
    return redirect(url_for('index'))
