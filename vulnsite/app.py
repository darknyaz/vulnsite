import os
from flask import Flask
from vulnsite import db, auth, blog

def setup_app(app):
    app.config.from_mapping(
        SECRET_KEY="strongkey",
        DB_PATH=os.path.join(app.instance_path, 'vulnsite.sqlite')
    )


    db.init_app(app)

    app.register_blueprint(auth.bp)
    app.register_blueprint(blog.bp)

    app.add_url_rule("/", endpoint="index")
    
application = Flask(__name__)
setup_app(application)

if __name__ == "__main__":
    application.run()
